import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../../actions';

import List from '../List';

class App extends Component {
  async componentDidMount() {
    const { fetchUsers } = this.props;
    fetchUsers();
  }

  render() {
    return (
      <List />
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchUsers: () => dispatch(actions.fetchUsers()),
});

App.propTypes = {
  fetchUsers: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(App);
