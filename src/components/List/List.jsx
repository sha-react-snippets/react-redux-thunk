/* eslint-disable react/prefer-stateless-function */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../../actions';

class List extends Component {
  render() {
    const { users, removeUser } = this.props;

    return (
      (users && users.length > 0)
        ? users.map((user) => (
          <div key={user.id}>
            <h3>{user.email}</h3>
            <button
              onClick={() => removeUser(user.id)}
              type="button"
            >
              Delete
            </button>
          </div>
        ))
        : <h3>No users found</h3>
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users.users,
});

const mapDispatchToProps = (dispatch) => ({
  removeUser: (id) => dispatch(actions.removeUser(id)),
});

List.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape({
    length: PropTypes.number,
    map: PropTypes.func,
  })).isRequired,
  removeUser: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
