import axios from 'axios';

// Users
export const STORE_USERS = 'STORE_USERS';
export const REMOVE_USER = 'REMOVE_USER';

export const storeUsers = (users) => ({
  type: STORE_USERS,
  payload: users,
});

export const removeUser = (id) => ({
  type: REMOVE_USER,
  payload: id,
});

export const fetchUsers = () => async (dispatch) => {
  const response = await axios.get('https://jsonplaceholder.typicode.com/users');
  dispatch(storeUsers(response.data));
};
